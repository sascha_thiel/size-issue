package de.sphw.samples;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.stream.IntStream;

public class App extends Application {
    public void start(String[] args) {
        launch(args);
    }

    private ObservableList<ListItemModel> items = FXCollections.observableArrayList();
    private ObservableList<String> selects = FXCollections.observableArrayList();
    private StringProperty selectedItem = new SimpleStringProperty();

    @Override
    public void start(Stage primaryStage) throws Exception {

        Pane rootNode = new VBox();

        setupDummyData();
        addUiElements(rootNode);

        Scene scene = new Scene(rootNode);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Size Issue");
        primaryStage.setWidth(500);
        primaryStage.setHeight(200);

        primaryStage.show();
    }

    private void setupDummyData() {
        IntStream.range(0, 10)
                .mapToObj(value -> {
                    ListItemModel item = new ListItemModel();
                    item.text = String.format("This is the line number %d", value);
                    item.colorGroup = value % 2 == 0 ? "Colored" : "";
                    return item;
                })
                .forEach(items::add);

        selects.addAll("Colored", "Not Colored");
    }

    private void addUiElements(Pane rootPane) {

        ToolBar toolBar = new ToolBar();
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setItems(selects);
        comboBox.getSelectionModel().selectFirst();
        comboBox.valueProperty().bindBidirectional(selectedItem);
        toolBar.getItems().add(comboBox);

        ListView<ListItemModel> listView = new ListView<>();
        listView.setCellFactory(param -> new ListItemCell(selectedItem));
        listView.setItems(items);
        rootPane.getChildren().addAll(toolBar, listView);

    }
}
