package de.sphw.samples;

import javafx.beans.Observable;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;

/**
 * @author Sascha Thiel
 */
public class ListItemCell extends ListCell<ListItemModel> {

    private final StringProperty selectedGroup;

    private HBox rootNode;
    private HBox message;
    private ContextMenu contextMenu;

    public ListItemCell(StringProperty selectedGroup) {
        this.selectedGroup = selectedGroup;

        contextMenu = new ContextMenu();
        MenuItem menuItem = new MenuItem("investigate");
        menuItem.setOnAction(this::investigateCell);
        contextMenu.getItems().add(menuItem);

        int size = 13;

        rootNode = new HBox();
        rootNode.setAlignment(Pos.CENTER_LEFT);
        rootNode.setSpacing(5);
        rootNode.setStyle("-fx-font-size: " + size + " px; -fx-font-family: Consolas;");

        rootNode.setOnContextMenuRequested(event -> contextMenu.show(rootNode, event.getScreenX(), event.getScreenY()));

        message = new HBox();
        message.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(message, Priority.ALWAYS);

        rootNode.getChildren().addAll(message);
        setGraphic(rootNode);
    }

    private void investigateCell(ActionEvent actionEvent) {
        System.out.println("message childs:");
        message.getChildren().forEach(node -> System.out.println(String.format("child '%s' -> pref w/h: %.2f/%.2f",
                node,
                node.prefWidth(-1), node.prefHeight(-1)
        )));
        System.out.println(String.format("message -> w/h: %.2f/%.2f, pref w/h: %.2f/%.2f",
                message.getWidth(), message.getHeight(),
                message.prefWidth(-1), message.prefHeight(-1)
        ));
        System.out.println(String.format("rootNode -> w/h: %.2f/%.2f, pref w/h: %.2f/%.2f",
                rootNode.getWidth(), rootNode.getHeight(),
                rootNode.prefWidth(-1), rootNode.prefHeight(-1)
        ));
        System.out.println(String.format("list cell -> w/h: %.2f/%.2f, pref w/h: %.2f/%.2f",
                getWidth(), getHeight(),
                getPrefWidth(), getPrefHeight()
        ));
    }

    @Override
    protected double computeMinHeight(double width) {
        return message.minHeight(width) + message.getInsets().getTop() + message.getInsets().getBottom();
    }

    @Override
    protected double computePrefHeight(double width) {
        return message.prefHeight(width) + message.getInsets().getTop() + message.getInsets().getBottom();
    }

    @Override
    protected void updateItem(ListItemModel item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        message.getChildren().clear();
        selectedGroup.removeListener(this::applyStyle);
        if (empty || item == null) {
            return;
        }
        selectedGroup.addListener(this::applyStyle);
        message.getChildren().add(new Text(item.text));
        applyStyle(null);
    }

    public void applyStyle(Observable observable) {
        if (getItem() == null) {
            return;
        }
        if (getItem().colorGroup.equals(selectedGroup.get())) {
            message.getChildren().forEach(node -> node.setStyle("-fx-fill: red; -fx-text-fill: red;"));
        } else {
            message.getChildren().forEach(node -> node.setStyle(null));
        }
    }
}
