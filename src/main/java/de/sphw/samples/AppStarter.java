package de.sphw.samples;

/**
 * @author Sascha Thiel
 */
public class AppStarter {

    public static void main(String[] args) {
        App app = new App();
        app.start(args);
    }
}
